from datetime import time, datetime
from typing import Union, List

from pydantic import BaseModel


class Area(BaseModel):
    name: str
    minimum_fl: Union[int, str]
    maximum_fl: Union[int, str]
    start_time: time
    end_time: time


class Info(BaseModel):
    type: str
    valid_wef: datetime
    valid_til: datetime
    released_on: datetime


class Output(BaseModel):
    info: Info
    areas: List[Area]
