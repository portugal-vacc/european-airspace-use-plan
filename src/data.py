import re
from datetime import time, datetime
from typing import List, Tuple

from src.models import Output, Area, Info


def regex_info(text: str) -> dict:
    """Match	Type EUUP
        Valid WEF 14/03/2022 13:00
        Valid TIL 15/03/2022 06:00
        Released On 14/03/2022 13:20
    Group 1 EUUP
    Group 2 14/03/2022 13:00
    Group 3 15/03/2022 06:00
    Group 4 14/03/2022 13:20
    """
    return re.search(
        r"Type (?P<type>\S{4})\nValid WEF (?P<valid_wef>\d{2}/\d{2}/\d{4} \d{2}:\d{2})\nValid TIL (?P<valid_til>\d{"
        r"2}/\d{2}/\d{4} \d{2}:\d{2})\nReleased On (?P<released_on>\d{2}/\d{2}/\d{4} \d{2}:\d{2})",
        text).groupdict()


def regex_data(text: str) -> List[Tuple[str]]:
    """Match ECNPZ3     095 660 12:00 06:00
    Group 1 ECNPZ3
    Group 2 095
    Group 3 660
    Group 4 12:00
    Group 5 06:00"""
    return re.findall(r"(\S{2,}).*(\S{3}).(\S{3}).(\d{2}:\d{2}).(\d{2}:\d{2})", text)


def parse_info(data: dict) -> Info:
    return Info(type=data["type"],
                valid_wef=datetime.strptime(data["valid_wef"], "%d/%m/%Y %H:%M"),
                valid_til=datetime.strptime(data["valid_til"], "%d/%m/%Y %H:%M"),
                released_on=datetime.strptime(data["released_on"], "%d/%m/%Y %H:%M"))


def parse_area(data: Tuple[str]) -> Area:
    return Area(name=data[0],
                minimum_fl=data[1],
                maximum_fl=data[2],
                start_time=time.fromisoformat(data[3]),
                end_time=time.fromisoformat(data[4]))


def convert_to_json(info: str, data: str) -> str:
    info_dict: dict = regex_info(info)
    info_model: Info = parse_info(info_dict)
    data_list: List[Tuple[str]] = regex_data(data)
    area_list: List[Area] = [parse_area(area) for area in data_list]
    output = Output(info=info_model, areas=area_list)
    return output.json()
