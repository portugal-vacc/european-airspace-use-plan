from typing import Dict

from src.data import convert_to_json
from src.web import web_latest_area_data


def main():
    data: Dict[str, str] = web_latest_area_data()
    data_json: str = convert_to_json(data["info"], data["data"])
    print(data_json)


if __name__ == "__main__":
    main()
